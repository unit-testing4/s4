const express = require('express');
const app = express();
const PORT = 4000;

app.use(express.json());

let userRoutes = require('./routes/routes');
app.use('/users',userRoutes);

app.listen(PORT, ()=>{

	console.log('Running on port ' + PORT)

})