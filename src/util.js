function getCircleArea(radius){
    if(radius<=0){
        return undefined;
    }else if(typeof(radius) != "number"){
        return undefined;
    }
        else{
        return 3.1416*(radius**2);
    }
}

function getNumberOfChar(char,string){

    if(typeof(char) != "string" || typeof(string) != "string"){
        return undefined
    }else{
        // transform the string into an array of characters so we can check the given char parameter against each character in the parameter.
        // .split("") - splits the characters is a string and returns them in a new array
        let characters = string.split("");
        // console.log(characters);
        // loop over each Item in the characters array. IF the char param matches with the current character being iterated, we will increment the counter variable.
        let counter = 0;
        characters.forEach(character => {
            if (character === char){
                counter++;
            }
        })
        // log the counter in the console
        console.log(counter);
        return counter;
    }
}

let users = [	
	{
		username: "brBoyd87",
		password: "87brandon19"
	},
	{
		username: "tylerOfsteve",
		password: "stevenstyle75"
	}
]

module.exports = {
	getCircleArea,
    getNumberOfChar,
    users
}